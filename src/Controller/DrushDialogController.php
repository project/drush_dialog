<?php

namespace Drupal\drush_dialog\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\drush_dialog\DrushDialogLogRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Process\Process;

/**
 * Provides route responses for drush_dialog.module.
 */
class DrushDialogController extends ControllerBase {

  /**
   * The repository for drush_dialog_log DB table.
   *
   * @var \Drupal\drush_dialog\DrushDialogLogRepository
   */
  protected DrushDialogLogRepository $repository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('drush_dialog.repository'),
    );
  }

  /**
   * Construct a new controller.
   *
   * @param \Drupal\drush_dialog\DrushDialogLogRepository $repository
   *   The repository service.
   */
  public function __construct(DrushDialogLogRepository $repository) {
    $this->repository = $repository;
  }

  /**
   * Outputs the result of the drush command.
   *
   * @param string $command
   *   The drush command.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The json response.
   *
   * @throws \Exception
   */
  public function run(string $command): JsonResponse {
    $params = explode(' ', $command);
    // ['drush', $command]
    array_unshift($params, 'drush');

    $process = new Process($params);

    $process->enableOutput();
    $process->run();

    $output = $process->getErrorOutput();
    if ($process->isSuccessful()) {
      $output = $process->getOutput();
    }

    $this->saveDrushLog($command, $output);

    return new JsonResponse($output);
  }

  /**
   * Outputs the list of previously runned drush commands.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The json response.
   */
  public function commandsList(): JsonResponse {
    $list = $this->repository->load($this->currentUser()->id());

    return new JsonResponse($list);
  }

  /**
   * Saves drush command with output to the DB log.
   *
   * @param string $command
   *   The drush command.
   * @param string $output
   *   The output of the drush command.
   *
   * @return void
   *   Nothing to return.
   *
   * @throws \Exception
   */
  private function saveDrushLog(string $command, string $output): void {
    $this->repository->insert([
      'uid' => $this->currentUser()->id(),
      'created' => time(),
      'command' => $command,
      'output' => $output,
    ]);
  }

}
