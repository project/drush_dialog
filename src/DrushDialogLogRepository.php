<?php

namespace Drupal\drush_dialog;

use Drupal\Core\Database\Connection;
use Drupal\Core\Utility\Error;
use Psr\Log\LoggerInterface;

/**
 * Repository for database-related helper methods for drush_dialog_log DB table.
 */
class DrushDialogLogRepository {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Construct a repository object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(Connection $connection, LoggerInterface $logger) {
    $this->connection = $connection;
    $this->logger = $logger;
  }

  /**
   * Save an entry in the database.
   *
   * @param array $entry
   *   An array containing all the fields of the database record.
   *
   * @return int|null
   *   The number of updated rows.
   *
   * @throws \Exception
   *   When the database insert fails.
   */
  public function insert(array $entry): int|null {
    try {
      $return_value = $this->connection->insert('drush_dialog_log')
        ->fields($entry)
        ->execute();
    }
    catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    return $return_value ?? NULL;
  }

  /**
   * Read from the database using a filter array.
   *
   * @param int $uid
   *   The uid of the user.
   *
   * @return array
   *   An array of drush commands.
   *
   * @see Drupal\Core\Database\Connection::select()
   */
  public function load(int $uid): array {
    $select = $this->connection
      ->select('drush_dialog_log')
      ->condition('uid', $uid)
      ->orderBy('created', 'DESC')
      ->range(0, 100)
      ->fields('drush_dialog_log', ['command']);

    return $select->execute()->fetchCol();
  }

}
