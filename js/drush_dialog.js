/**
 * @file
 * JavaScript file for the drush_dialog module.
 */

(function ($, Drupal, drupalSettings, Drupaldrush_dialog) {

  'use strict';

  /**
   * drush_dialog module namespace.
   *
   * @namespace
   *
   * @todo put this in Drupal.drush_dialog to expose it.
   */
  Drupaldrush_dialog = Drupaldrush_dialog || {};

  /**
   * Attaches drush_dialog module behaviors.
   *
   * Initializes DOM elements drush_dialog module needs to display the search.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Attach drush_dialog functionality to the page.
   *
   * @todo get most of it out of the behavior in dedicated functions.
   */
  Drupal.behaviors.drush_dialog = {
    attach: function (context) {
      const body = once('drush_dialog', 'body', context);
      body.forEach((body) => {
        var $body = $(body);
        Drupaldrush_dialog.bg.appendTo($body).hide();
        Drupaldrush_dialog.wrapper.appendTo('body').addClass('hide-form');

        Drupaldrush_dialog.form
          .append(Drupaldrush_dialog.results)
          .append(Drupaldrush_dialog.label)
          .append(Drupaldrush_dialog.field)
          .append(Drupaldrush_dialog.loader)
          .wrapInner('<div id="drush_dialog-form-inner" />')
          .appendTo(Drupaldrush_dialog.wrapper);

        Drupaldrush_dialog.form.keydown(function (event) {
          // KEY Up
          if (event.keyCode === 38) {
            if (Drupaldrush_dialog.commandsListPointer <= Drupaldrush_dialog.commandsList.length) {
              var $el = Drupaldrush_dialog.commandsList[Drupaldrush_dialog.commandsListPointer++];
              Drupaldrush_dialog.field.val($el);
            } else {
              Drupaldrush_dialog.field.val('');
            }
          }
          // KEY DOWN
          if (event.keyCode === 40) {
            if (Drupaldrush_dialog.commandsListPointer > 0) {
              var $el = Drupaldrush_dialog.commandsList[--Drupaldrush_dialog.commandsListPointer];
              Drupaldrush_dialog.field.val($el);
            } else {
              Drupaldrush_dialog.field.val('');
            }
          }
          if (event.keyCode === 13) {
            var $command = Drupaldrush_dialog.field.val();
            if ($command.length) {
              Drupaldrush_dialog.field.val('');
              Drupaldrush_dialog.field.addClass('hidden');
              Drupaldrush_dialog.loader.removeClass('hidden');

              $.ajax({
                url: Drupal.url('drush-dialog/run/' + $command),
                dataType: 'json',
                success: function (data) {
                  const d = new Date();
                  let time = d.getTime();
                  var $responce = "<pre id='" + time + "'>" +
                    "<div class='drush-command'>" +
                    "$ drush " + $command +
                    "</div>" +
                    data + "</pre>";
                  Drupaldrush_dialog.results.append($responce);

                  Drupaldrush_dialog.loader.addClass('hidden');
                  Drupaldrush_dialog.field.removeClass('hidden');
                  Drupaldrush_dialog.field.focus();

                  document.getElementById(time).scrollIntoView({ behavior: 'smooth', block: 'end' });

                  Drupaldrush_dialog.commandsList.unshift($command);
                  Drupaldrush_dialog.commandsListPointer = 0;
                },
                error: function () {
                  Drupaldrush_dialog.field.val('Could not load data, please refresh the page');
                }
              });
            }
          }
        });

        $(document).keydown(function (event) {

          // Show the form with ctrl + D. Use 2 keycodes as 'D' can be uppercase or lowercase.
          if (Drupaldrush_dialog.wrapper.hasClass('hide-form') &&
            event.ctrlKey === true &&
              // 68/206 = d/D, 75 = k.
            (event.keyCode === 68 || event.keyCode === 206 || event.keyCode === 75)) {
            Drupaldrush_dialog.drush_dialog_show();
            Drupaldrush_dialog.commands_list_load();
            event.preventDefault();
          }
          // Close the form with esc or ctrl + D.
          else {
            if (!Drupaldrush_dialog.wrapper.hasClass('hide-form') && (event.keyCode === 27 || (event.ctrlKey === true && (event.keyCode === 68 || event.keyCode === 206)))) {
              Drupaldrush_dialog.drush_dialog_close();
              event.preventDefault();
            }
          }
        });
      });
    }
  };

  /**
   * Open the form and focus on the search field.
   */
  Drupaldrush_dialog.drush_dialog_show = function () {
    Drupaldrush_dialog.wrapper.removeClass('hide-form');
    Drupaldrush_dialog.bg.show();
    Drupaldrush_dialog.field.focus();
  };

  /**
   * Close the form and destroy all data.
   */
  Drupaldrush_dialog.drush_dialog_close = function () {
    Drupaldrush_dialog.field.val('');
    Drupaldrush_dialog.wrapper.addClass('hide-form');
    Drupaldrush_dialog.bg.hide();
  };

  /**
   * The HTML elements of the dialog.
   */
  Drupaldrush_dialog.label = $('<label for="drush_dialog-q" class="container-inline"/>').text(Drupal.t('$ drush ', '', ''));
  Drupaldrush_dialog.results = $('<pre id="drush_dialog-results" />');
  Drupaldrush_dialog.wrapper = $('<div class="drush_dialog-form-wrapper" />');
  Drupaldrush_dialog.loader = $('<div class="spinner container-inline hidden">\n' +
    '  <div class="bounce1"></div>\n' +
    '  <div class="bounce2"></div>\n' +
    '  <div class="bounce3"></div>\n' +
    '</div>');
  Drupaldrush_dialog.form = $('<form id="drush_dialog-form" action="#" />');
  Drupaldrush_dialog.bg = $('<div id="drush_dialog-bg" />').click(function () {
    Drupaldrush_dialog.drush_dialog_close();
  });

  Drupaldrush_dialog.field = $('<input id="drush_dialog-q" type="text" class="container-inline" />');

  Drupaldrush_dialog.commandsList = [];
  Drupaldrush_dialog.commandsListPointer = 0;
  Drupaldrush_dialog.commands_list_load = function () {
    $.ajax({
      url: Drupal.url('drush-dialog/commands-list/'),
      dataType: 'json',
      success: function (data) {
        Drupaldrush_dialog.commandsList = data;
        Drupaldrush_dialog.commandsListPointer = 0;
      },
      error: function () {
        Drupaldrush_dialog.commandsList = [];
      }
    });
  };

})(jQuery, Drupal, drupalSettings);
