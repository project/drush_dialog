
-- SUMMARY --

The module allows you to run drush commands from the UI dialog on the site.
Press Ctrl + d to display the drush dialog.


-- REQUIREMENTS --

Drush should be installed and configured.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70501 for further information.


-- USAGE --

Use keyboard shortcut ctrl + d

